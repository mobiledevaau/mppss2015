# Information about the Mobile Phone Programming summerschool 2015

The purpose of this repository is to host our wiki and files used during
the Mobile Phone Programming summerschool 2015.

* Go to the [wiki](https://bitbucket.org/mobiledevaau/mppss2015/wiki) for an overview.